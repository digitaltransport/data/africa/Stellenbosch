The GTFS dataset for Stellenbosch city is collected by GoMetro with a funding from the [DigitalTransport4Africa (DT4A) Innovation Challenge](https://digitaltransport4africa.org/innovation-challenge/). The dataset was used as an input to assess the viability of transitioning South African minibus taxis to environmentally-friendly electric vehicles. The dataset encompasses operational data from 10 minibus taxis that operate in Stellenbosch, South Africa, and covers a total of 30 complete operating days. The data was collected over a period spanning from 8 March 2023 to 18 March 2023.

- _**Stellenbosch_GTFS_V1**_ Contains a GTFS dataset with stops (unclustered)
- _**Stellenbosch_GTFS_V2**_ Contains a GTFS dataset with clustered trips (shapes) and reduced stops
- _**Stellenbosch_GTFS_V3**_ Contains a GTFS dataset with clustered trips (shapes) and stops

